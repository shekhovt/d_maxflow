reading split ../../test/d_maxflow/BVZ-tsukuba0.max_reg/rgraph
page size required: 20.763Mb
Regions: 16
sweep: 0 flow: 34513 decided:  0.0% active: 12  changed: 15 relabelled: 0 CPU: 0.100339sec.
sweep: 1 flow: 34653 decided:  0.0% active: 8  changed: 12 relabelled: 0 CPU: 0.00246177sec.
sweep: 2 flow: 34653 decided:  0.0% active: 7  changed: 12 relabelled: 0 CPU: 0.0017871sec.
sweep: 3 flow: 34669 decided:  0.0% active: 5  changed: 11 relabelled: 0 CPU: 0.00199159sec.
sweep: 4 flow: 34669 decided:  0.0% active: 1  changed: 9 relabelled: 0 CPU: 0.00126636sec.
sweep: 5 flow: 34669 decided:  0.0% active: 0  changed: 9 relabelled: 0 CPU: 0.000226565sec.
flow is optimal
sweep: 6 flow: 34669 decided:  0.0% active: 0  changed: 4 relabelled: 0 CPU: 0.00200081sec.
sweep: 7 flow: 34669 decided:  0.0% active: 0  changed: 0 relabelled: 0 CPU: 0.00116886sec.
Total CPU: 0.111242
solver: S-PRD
vert: 110594
edges: 220512
bnd_edges: 2016
mem_shared:  0.5Mb
mem_region:  0.8Mb
flow: 34669
sweeps: 8
construct: 0.28625
    solve: 1.38104
      msg: 0.00151947
  augment: 0.0938323
  relabel: 0.00782753
      gap: 0.00806276
disk read: 0.0175408s / 40.9Mb
disk writ: 1.25904s / 53.1Mb
_____________________________________________
All Ok
