reading split ../../test/d_maxflow/BVZ-tsukuba0.max_reg/rgraph
mem required (regions): 20.6545Mb
Regions: 16
sweep: 0 flow: 34527 decided:  0.0% active: 16  changed: 16 relabelled: 3460 CPU: 0.0184674sec.
sweep: 1 flow: 34539 decided:  0.0% active: 16  changed: 16 relabelled: 3997 CPU: 0.0016368sec.
sweep: 2 flow: 34669 decided:  0.0% active: 0  changed: 0 relabelled: 0 CPU: 0.00164462sec.
flow is optimal
Total CPU: 0.0217489
solver: S-ARD1
vert: 110594
edges: 220512
bnd_edges: 2016
mem_shared:  0.2Mb
mem_region:  0.6Mb
flow: 34669
sweeps: 3
construct: 0.554476
    solve: 0.355119
      msg: 0.0013186
  augment: 0.0186247
  relabel: 0.00094593
      gap: 0.000859606
disk read: 0.012001s / 19.1Mb
disk writ: 0.320346s / 28.7Mb
_____________________________________________
All Ok
