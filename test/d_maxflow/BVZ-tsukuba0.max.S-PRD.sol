reading split ../../test/d_maxflow/BVZ-tsukuba0.max_reg/rgraph
mem required (regions): 20.7906Mb
Regions: 16
sweep: 0 flow: 34513 decided:  0.0% active: 12  changed: 15 relabelled: 0 CPU: 0.103904sec.
sweep: 1 flow: 34653 decided:  0.0% active: 8  changed: 12 relabelled: 0 CPU: 0.00259754sec.
sweep: 2 flow: 34653 decided:  0.0% active: 7  changed: 12 relabelled: 0 CPU: 0.00186644sec.
sweep: 3 flow: 34669 decided:  0.0% active: 5  changed: 12 relabelled: 0 CPU: 0.00192818sec.
sweep: 4 flow: 34669 decided:  0.0% active: 1  changed: 10 relabelled: 0 CPU: 0.00139906sec.
sweep: 5 flow: 34669 decided:  0.0% active: 0  changed: 9 relabelled: 0 CPU: 0.000255619sec.
flow is optimal
sweep: 6 flow: 34669 decided:  0.0% active: 0  changed: 6 relabelled: 0 CPU: 0.00431479sec.
sweep: 7 flow: 34669 decided:  0.0% active: 0  changed: 0 relabelled: 0 CPU: 0.00197567sec.
Total CPU: 0.118241
solver: S-PRD
vert: 110594
edges: 220512
bnd_edges: 2016
mem_shared:  0.6Mb
mem_region:  0.8Mb
flow: 34669
sweeps: 8
construct: 0.301034
    solve: 1.14973
      msg: 0.00170748
  augment: 0.0987497
  relabel: 0.00892991
      gap: 0.0088542
disk read: 0.0285103s / 48.7Mb
disk writ: 1.01068s / 61.3Mb
_____________________________________________
All Ok
